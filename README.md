# Himydata Library #

### Requirement ###

***

* SQLAlchemy
* Pandas
* Himydata backend up
* Setting Himydata backend's url in Setting.py

### Installation ###

***
Install this library with :

````
pip install path/himydata
````

## Utilisation ##

***

#### Get a dataset ####

This library gives a object representation of a dataset stored in HMD_WAREHOUSE.

You can get a dataset from 2 manners : 

* The first one is to get an existing dataset :
    ``` python
    dataset = Dataset(name, api_key, company_id, schema, user, read_only)
    ```
  
  * name: The name of the dataset you want,
  * api_key: Your api key for the himydata backend,
  * company_id: The number of your company,
  * schema: _Optional_ - The schema were the dataset is stored (By default is set to "public"),
  * user: _Optional_ - The current user (By default is set to None),
  * read_only: _Optional_ - Define if the dataset is in read only mode (By default is set to True),


* The second one is to create a dataset from a dataframe :
    ``` python
    dataset = Dataset.create_ds(project_id, company_id, dataframe, name, apikey, schema, read_only)
    ```
  
  * project_id: The UUID of the project of the dataset,
  * company_id: The number of your company,
  * dataframe: The pandas dataframe you want convert into dataset,
  * name: The name of the dataset you want to create,
  * api_key: Your api key for the himydata backend,
  * schema: _Optional_ - The schema were the dataset is stored (By default is set to "public"),
  * read_only: _Optional_ - Define if the dataset is in read only mode (By default is set to True),

#### Methods ####

* #### get_schema()
  Get the schema (fields) of the dataset :

  ```python
  fields = [] 
  fields = dataset.get_schema() 
  ```
  
  Return a list containing all the fields of you the dataset.


* #### get_rows(head,limit)
  Get all the rows of the dataset depending on the limit. If limit is set to 10, we return only the 10 first rows.
  The head is optional and define which column we need to return :
  
  ```python
    rows = dataset.get_rows()
    first_10_rows = dataset.get_rows(limit = 10)
    rows_with_only_x_and_y_column = dataset.get_rows(heads=[x,y])
  ```
  
  * heads: _Optional_ - A list defining which columns we keep (By default, we keep all the column),
  * limit: _Optional_ - A integer defining how many rows we return (By default, we don't put limit),
  
  Return a SQLAlchemy object which contains all the rows

* #### get_rows_by_value(filter, heads,limit)
  Get all the rows of the dataset depending on the filter and the limit. If limit is set to 10, we return only the 10 first rows.
  The head is optional and define which column we need to return :

  ```python
    filters = [
    {"column":column_name,
     "operator":operator,
     "value":value
    },
    {"column":"age",
     "operator":">=",
     "value":6
    },
    ]
  
    rows = dataset.get_rows_by_value(filters)
    first_10_rows = dataset.get_rows_by_value(values, limit = 10)
    rows_with_only_x_and_y_column = dataset.get_rows_by_value(values, heads=[x,y])
  ```
  
  * filter: A list of all condition to respect when we get the row, 
    * condition :```{"column":column_name,"operator":operator,"value":value}```
  * heads: _Optional_ - A list defining which columns we keep (By default, we keep all the column),
  * limit: _Optional_ - A integer defining how many rows we return (By default, we don't put limit),

  Return a SQLAlchemy object which contains all the rows

* #### get_df(heads, limit, index, parse_date)

  Get all the rows and convert them into a dataframe. the length of the dataframe depend on the limit. If limit is set to 10, we return only the 10 first rows.
  Heads is optional and define which column we need to return. Index and parse_date are the argument to give to pandas, they define which column of the dataset
  will be the index (or none of them if set to None) and parse_date define which column is a date and how to parse it:
  ```python
  dataframe_x_y = dataset.get_df(heads=[x,y])
  dataframe_10_first_rows = dataset.get_df(limit=10)
  dataframe_index_column = dataset.get_df(index="column")
  date_parser = {"column":"date", "format":"%Y-%d-%m %H:%M:%S"}
  dataframe_date_parsed = dataset.get_df(parse_date=date_parser)
  ```
  
  * heads: _Optional_ - A list defining which columns we keep (By default, we keep all the column),
  * limit: _Optional_ - A integer defining how many rows we return (By default, we don't put limit),
  * index: _Optional_ - The name of the column which will be the dataframe's index (By default is set to None),
  * parse_date: _Optional_ - A dictionary defining which column to parse into date and in which format, 
    * format :```{"column":column_name,"format":date_format}``` 
  
  Return a panda's dataframe containing the rows of the dataset.

* #### get_df_by_value(filter, heads, limit, index, parse_date)

  Get all the rows filtered and convert them into a dataframe. the length of the dataframe depend on the limit. If limit is set to 10, we return only the 10 first rows.
  Heads is optional and define which column we need to return. Index and parse_date are the argument to give to pandas, they define which column of the dataset
  will be the index (or none of them if set to None) and parse_date define which column is a date and how to parse it:
  ```python
  filter = [{"column":"date","operator":">","value":StartDate},
          {"column":"date","operator":"<","value":EndDate}]
  date_parser = {"column":"date","format":"%Y-%d-%m %H:%M:%S"}
  dataframe = dataset.get_df_by_value(filter, heads=var_list, limit=10, index = "date", parse_date=date_parser)
  ```
  
  * filter: A list of all condition to respect when we get the row, 
    * condition :```{"column":column_name,"operator":operator,"value":value}```
  * heads: _Optional_ - A list defining which columns we keep (By default, we keep all the column),
  * limit: _Optional_ - A integer defining how many rows we return (By default, we don't put limit),
  * index: _Optional_ - The name of the column which will be the dataframe's index (By default is set to None),
  * parse_date: _Optional_ - A dictionary defining which column to parse into date and in which format, 
    * format :```{"column":column_name,"format":date_format}```
  
  Return a panda's dataframe containing the rows filtered of the dataset.
  
* #### insert_df_into_table(dataframe)

  Append all data from dataframe to the dataset
  ```python
    dataframe = pandas.DataFrame(data={"name": ["toto","titi"], "age": [20,21]})
    dataset.insert_df_into_table(dataframe)
  ```
  
  * dataframe: The pandas dataframe containing all the data to appends into the dataset,
  
  Return True when the operation is finished

* #### insert_into_table(value_to_insert)

  Append a value into a dataframe
  ```python
    value_to_insert = {"name": "toto", "age":20}
    dataset.insert_into_table(value_to_insert)
  ```
  
  * value_to_insert: The value to insert into the dataset,
    * value: ```{column_name: value_to_insert,...} #for each column name in the dataset```
  
  Return True when the operation is finished

* #### update_rows(value, condition)

  Modify all value of specified column depending on condition
  ```python
    condition = [{"column":"age","operator":">","value":"18"}]
    value = {"majeur": true,}
    dataset.update_rows(value, condition)
  ```
  
  * value: The new value to put into the column,
    * value: ```{column_name: new_value,...} # put each column you want to update```
  * condition: _Optional_ - The list of filter to define where to update value (By default we change the value of the column for all rows),
    * filter:```{"column":column_name,"operator":operator,"value":value}```
  
  Return True when the update is finished

* #### delete_rows(condition_line_to_delete)

  Delete all rows which respect the condition. Warning, if the condition is not set, it will delete all the rows of your dataset
  ```python
    condition = [{"column":"age","operator":">","value":"18"}]
    dataset.delete_rows(condition)
  ```
  
  * condition: _Optional_ - The list of filter to define where to update value (By default we change the value of the column for all rows),
    * filter:```{"column":column_name,"operator":operator,"value":value}```
  
  Return True when the delete is finished

## Example 

***

#### Code: 

```python
from himydata import Dataset
import pandas 

dataframe = pandas.DataFrame(data={"name": ["toto","titi","tata"], "age": [20,21,22], "birthday":["2002-01-10","2001-01-11","2000-01-12"]})
ds = Dataset.create_ds("feacc9cd-d269-4da6-925b-517d4ac391bd", 1,dataframe, "dataset_himydata_test", "2ae4c28c530c5b144a252d8ebecee14aa15d8851",schema="hmd_2122ddf4a5794d89bb2ed81e1e91d50c", read_only = False)

print("--------Schema---------")
print(ds.get_schema())

print("--------Row---------")
rows = ds.get_rows()
for row in rows:
    print(row)
    
print("--------DF - Name, Birthday--------")
filter = [{"column":"age","operator":">","value":20},{"column":"age","operator":"<","value":22}]
df = ds.get_df_by_value(filter, heads=["name","birthday"], index="birthday", parse_date={"column":"birthday","format":"%Y-%m-%d"})
print(df)

print("--------Insert DF---------")
dataframe = pandas.DataFrame(data={"name": ["tutu"], "age": [23], "birthday":["1999-02-13"]})
ds.insert_df_into_table(dataframe)
df = ds.get_df(index="index")
print(df)

print("--------Insert Value---------")
value = {"name":"tyty","age":24,"birthday":"1998-02-14"}
ds.insert_into_table(value)
df = ds.get_df(index="index")
print(df)

print("---------Update Rows--------")
value = {"name": "tete"}
condition = [{"column":"age","operator":">","value":22}]
ds.update_rows(value, condition)
df = ds.get_df(index="index")
print(df)

print("--------Delete Rows---------")
condition = [{"column":"age","operator":">","value":22}]
ds.delete_rows(condition)
df = ds.get_df(index="index")
print(df)
print("-----------------")
```

#### Output:

```python
--------Schema---------
['index', 'name', 'age', 'birthday']
--------Row---------
(0, 'toto', 20, '2002-01-10')
(1, 'titi', 21, '2001-01-11')
(2, 'tata', 22, '2000-01-12')
--------DF - Name, Birthday--------
            name
birthday        
2001-01-11  titi
--------Insert DF---------
       name  age    birthday
index                       
0      toto   20  2002-01-10
1      titi   21  2001-01-11
2      tata   22  2000-01-12
0      tutu   23  1999-02-13
--------Insert Value---------
       name  age    birthday
index                       
0.0    toto   20  2002-01-10
1.0    titi   21  2001-01-11
2.0    tata   22  2000-01-12
0.0    tutu   23  1999-02-13
NaN    tyty   24  1998-02-14
---------Update Rows--------
       name  age    birthday
index                       
0.0    toto   20  2002-01-10
1.0    titi   21  2001-01-11
2.0    tata   22  2000-01-12
0.0    tete   23  1999-02-13
NaN    tete   24  1998-02-14
--------Delete Rows---------
       name  age    birthday
index                       
0      toto   20  2002-01-10
1      titi   21  2001-01-11
2      tata   22  2000-01-12
-----------------
```

## Todo

***

* A method to update the dataset itself (truncate, add column, etc...)
* A method to delete the dataset

****
API client for Himydata Platform

For more information, see:
https://doc.himydata.com/index.html