DATAWAREHOUSE_SETTINGS = {
    "drivername": "postgresql",
    "username": "hmd_dashboard_user",
    "password": "hmd_dashboard_user",
    "host": "127.0.0.1",
    "port": "5432",
    "database": "hmd_datawarehouse",
}

DASHBOARD_SETTINGS = {
    "drivername": "postgresql",
    "username": "hmd_dashboard_user",
    "password": "hmd_dashboard_user",
    "host": "127.0.0.1",
    "port": "5432",
    "database": "hmd_dashboard",
}

HMD_URL = "http://localhost:8002"
