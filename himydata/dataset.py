import sqlalchemy as sa
import pandas
from himydata.Setting import DATAWAREHOUSE_SETTINGS, DASHBOARD_SETTINGS
from himydata.utils.utils import *
from sqlalchemy.pool import NullPool


class Dataset(object):
    """
    A dataset on the HMD instance
    """

    def __init__(self, name, api_key, company_id, schema="public", user=None, read_only=True):
        self.user = user
        self.db_name = name.lower()
        self.company_id = company_id
        self.read_only = read_only
        self.api_key = api_key
        self.schema = schema
        sa_uri = sa.engine.url.URL(drivername=DATAWAREHOUSE_SETTINGS["drivername"],
                                   username=DATAWAREHOUSE_SETTINGS["username"],
                                   password=DATAWAREHOUSE_SETTINGS["password"], host=DATAWAREHOUSE_SETTINGS["host"],
                                   port=DATAWAREHOUSE_SETTINGS["port"], database=DATAWAREHOUSE_SETTINGS["database"])

        self.connection = sa.create_engine(sa_uri, connect_args={'options': F'-csearch_path={schema}'}).connect()

    @staticmethod
    def create_ds(project_id, company_id, df, name, apikey, schema="public", read_only=True):
        """
        Create a new dataset from a dataframe

        project_id : id of the project where will be the dataset
        company_id : id of the company which manage the dataset
        df : Dataframe to convert into dataset
        name : name of the dataset
        apikey : apikey to connect to the backend
        schema : where will be stored the dataset
        read_only : define if the dataset is read_only or not

        Returns:
            A dataset object connected to the dataset created
        """
        name = name.lower()
        sa_uri = sa.engine.url.URL(drivername=DATAWAREHOUSE_SETTINGS["drivername"],
                                   username=DATAWAREHOUSE_SETTINGS["username"],
                                   password=DATAWAREHOUSE_SETTINGS["password"], host=DATAWAREHOUSE_SETTINGS["host"],
                                   port=DATAWAREHOUSE_SETTINGS["port"], database=DATAWAREHOUSE_SETTINGS["database"])

        connection = sa.create_engine(sa_uri, connect_args={'options': F'-csearch_path={schema}'}).connect()

        dataset = {"description": "",
                   "is_global": False,
                   "is_private": False,
                   "labels": [],
                   "name": name,
                   "project_id": project_id}
        response = create_dataset(dataset, apikey)
        hmd_id = response.json().get("hmd_id")
        fields = compute_fields(df)
        dataset_field = {"hmd_id": hmd_id,
                         "name": name,
                         "fields": fields}
        fill_field_into_dataset(hmd_id, dataset_field, apikey)
        if df.index.name is not None:
            df.to_sql(name, connection, if_exists='replace', index=df.index.name, chunksize=100000)
        else:
            df.to_sql(name, connection, if_exists='replace', chunksize=100000)
        return Dataset(name, apikey, company_id, schema=schema, user=None, read_only=read_only)

    ####################
    # Dataset metadata #
    ####################
    def get_schema(self):
        """
        Get the schema of the dataset

        Returns:
            a list of columns in the dataset
        """
        sql = F"select column_name from INFORMATION_SCHEMA.COLUMNS where table_name = '{self.db_name}'"
        rows = self.connection.execute(sql)
        result = []
        for row in rows:
            result.append(row["column_name"])
        return result

    def execute(self, query):
        """
        query : query to execute on our database

        Returns:
            the result of the query on the schema
        """
        rows = self.connection.execute(query)
        return rows

    def get_rows(self, heads=None, limit=None):
        """
        Get rows from dataset. All if heads is none and only the heads if present
        Heads refers to the columns filtered, if None mentioned, shows all

        heads : list containing the columns of interest
        limit : limit the number of rows retrieved

        Returns:
            All rows of the dataset depending on the limit and head
        """
        query = get_query(self.db_name, heads=heads, limit=limit)
        rows = self.connection.execute(query)
        return rows

    def get_rows_by_value(self, values, heads=None, limit=None):
        """
        Get rows from dataset where value
        Heads refers to the columns filtered, if None mentioned, shows all

        heads : list with columns of interest
        limit : limit the number of rows retrieved
        values : the values which is our filter

        Returns:
            Rows where the condition defined in values are true
        """
        query = get_query(self.db_name, heads=heads, limit=limit, values=values)
        rows = self.connection.execute(query)
        return rows

    def get_df(self, heads=None, limit=None, index=None, parse_date=None):
        """
        Get rows from dataset. All if heads is none and only the heads if present
        Heads refers to the columns filtered, if None mentioned, shows all

        heads : list containing the columns of interest
        limit : limit the number of rows retrieved
        index : used to define which column will be the index of the dataframe
        parse_date : used to define which column is a date and how o read it ({"column":column_name,"format":format})

        Returns:
            pandas dataframe
        """
        query = get_query(self.db_name, heads=heads, limit=limit)
        if parse_date:
            parse_date = {parse_date["column"]: {"format": parse_date["format"]}}
            df = pandas.read_sql(query, self.connection, index_col=index, parse_dates=parse_date)
        else:
            df = pandas.read_sql(query, self.connection, index_col=index)
        return df

    def get_df_by_value(self, values, heads=None, limit=None, index=None, parse_date=None):
        """
        Get rows from dataset where value
        Heads refers to the columns filtered, if None mentioned, shows all

        heads : list with columns of interest
        limit : limit the number of rows retrieved
        values : the values which is our filter
        index : used to define which column will be the index of the dataframe
        parse_date : used to define which column is a date and how o read it ({"column":column_name,"format":format})

        Returns:
            pandas dataframe
        """
        query = get_query(self.db_name, heads=heads, limit=limit, values=values)

        if parse_date:
            parse_date = {parse_date["column"]: {"format": parse_date["format"]}}
            df = pandas.read_sql(query, self.connection, index_col=index, parse_dates=parse_date)
        else:
            df = pandas.read_sql(query, self.connection, index_col=index)
        return df

    def insert_df_into_table(self, dataframe):
        """
        Append all data from the dataframe to the dataset

        dataframe : the dataframe pandas to append in our dataset

        Returns:
            True when the process is finish
        """
        if self.read_only:
            raise Exception("You can't modify a read_only dataset")
        if dataframe.index.name is not None:
            dataframe.to_sql(self.db_name, self.connection, if_exists='append', index=dataframe.index.name, chunksize=100000)
        else:
            dataframe.to_sql(self.db_name, self.connection, if_exists='append', chunksize=100000)
        return True

    def insert_into_table(self, value_to_insert):
        """
        Append a value into the dataset

        value_to_insert : the value to append in our dataset ( {column_name:value} )

        Returns:
            True when the process is finish
        """
        if self.read_only:
            raise Exception("You can't modify a read_only dataset")
        if len(value_to_insert) < 1:
            return False
        keys = value_to_insert.keys()
        values = value_to_insert.values()
        keys_sql = ""
        values_sql = ""
        first = True
        for _ in keys:
            if not first:
                keys_sql += ","
            first = False
            keys_sql += F"{_} "
        first = True
        for _ in values:
            if not first:
                values_sql += ","
            first = False
            values_sql += F"'{_}' "
        sql = F"INSERT INTO {self.db_name} ({keys_sql}) VALUES ({values_sql})"
        self.connection.execute(sql)
        return True

    def update_rows(self, new_val, condition={}):
        """
        modify all value of specified column depending on condition

        new_val : the value to append in our dataset ( {column_name:value} )
        condition: condition to verified before updating column ( {column: column_name, operator: operator, value: value_to_compare} )

        Returns:
            True when the process is finish
        """
        if self.read_only:
            raise Exception("You can't modify a read_only dataset")
        cond = compute_value(condition)
        sql_assignement = ""
        first = True
        for _ in new_val:
            if not first:
                sql_assignement += ", "
            sql_assignement += F"{_} = '{new_val[_]}'"
            first = False
        sql = F"UPDATE {self.db_name} SET {sql_assignement} {cond}"
        self.connection.execute(sql)
        return True

    def delete_rows(self, condition_line_to_delete={}):
        """
        delete all data depending on condition

        condition_line_to_delete: condition to verified before deleting row ( {column: column_name, operator: operator, value: value_to_compare} )

        Returns:
            True when the process is finish
        """
        if self.read_only:
            raise Exception("You can't modify a read_only dataset")
        condition = compute_value(condition_line_to_delete)
        sql = F"DELETE FROM {self.db_name} {condition}"
        self.connection.execute(sql)
        return True

    # (Not Working) todo correcting delete in order to delete a dataset
    def delete_ds(self):
        """
        Delete the actual dataset

        Returns:
            True when the process is finish
        """
        sa_uri = sa.engine.url.URL(drivername=DASHBOARD_SETTINGS["drivername"],
                                   username=DASHBOARD_SETTINGS["username"],
                                   password=DASHBOARD_SETTINGS["password"], host=DASHBOARD_SETTINGS["host"],
                                   port=DASHBOARD_SETTINGS["port"], database=DASHBOARD_SETTINGS["database"])
        engine = sa.create_engine(sa_uri, connect_args={'options': F'-csearch_path={"public"}'}, poolclass=NullPool)
        connection = engine.connect()
        rows = connection.execute(
            F"SELECT hmd_id FROM dataset_dataset WHERE name='{self.db_name}' AND company_id ='{self.company_id}'")
        hmd = []
        for row in rows:
            hmd.append(str(row["hmd_id"]))
        connection.close()
        engine.dispose()
        for _ in hmd:
            delete_dataset(_, self.api_key)
        return True

    # (Not Working) todo correcting add_column in order to add a column to a dataset
    def add_column(self, new_column, type, required=False, primary_key=False, editable=True):
        """
        add a new column to the dataset

        new_column : list containing the columns of interest
        type : The type of the new column
        required : True if the column is requires
        primary_key : True if the column is a primary key
        editable : True if the column is editable
        """
        df = pandas.read_sql("SELECT * FROM %s " % self.db_name, self.connection)
        fields = compute_fields(df)
        new_field = {"editable": editable,
                     "exists": True,
                     "isNew": True,
                     "name": new_column,
                     "primary_key": primary_key,
                     "required": required,
                     "type": type,
                     "updatedName": new_column}
        fields.append(new_field)
        sa_uri = sa.engine.url.URL(drivername=DASHBOARD_SETTINGS["drivername"],
                                   username=DASHBOARD_SETTINGS["username"],
                                   password=DASHBOARD_SETTINGS["password"], host=DASHBOARD_SETTINGS["host"],
                                   port=DASHBOARD_SETTINGS["port"], database=DASHBOARD_SETTINGS["database"])
        engine = sa.create_engine(sa_uri, connect_args={'options': F'-csearch_path={"public"}'}, poolclass=NullPool)
        connection = engine.connect()
        rows = connection.execute(
            F"SELECT hmd_id FROM dataset_dataset WHERE name='{self.db_name}' AND company_id ='{self.company_id}'")
        hmd = []
        for row in rows:
            hmd.append(str(row["hmd_id"]))
        connection.close()
        engine.dispose()
        for hmd_id in hmd:
            dataset_field = {"hmd_id": hmd_id,
                             "name": self.db_name,
                             "fields": fields}
            fill_field_into_dataset(hmd_id, dataset_field, self.api_key)
