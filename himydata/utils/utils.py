import requests
import json

from himydata.Setting import HMD_URL


def create_dataset(dataset, apikey):
    """
    Send request to himydata backend to create a dataset

    dataset : the dataset to post to the backend
    apikey: the apikey

    Returns:
        Response from the rquest
    """
    url = HMD_URL + "/api/datastore/dataset/manage/"

    headers = {"Content-Type": "application/json; charset=utf-8",
               "authorization": F"Token {apikey}"}
    response = requests.post(url, headers=headers, json=dataset)
    return response


# (Not Working) todo repair this
def delete_dataset(hmd_id, apikey):
    """
   Send request to himydata backend to delete a dataset

   hmd_id : the id of the dataset to delete
   apikey: the apikey

   Returns:
       Response from the request
   """
    url = F'{HMD_URL}/api/datastore/dataset/manage/{hmd_id}/'

    headers = {"Content-Type": "application/json",
               "authorization": F"Token {apikey}"}
    response = requests.delete(url, headers=headers, timeout=10)


def fill_field_into_dataset(hmd_id, fields, apikey):
    """
   Send request to himydata backend to update the schema of a dataset

   hmd_id : the id of the dataset to delete
   fields: all fields of the dataset
   apikey: the apikey
   """
    url = F"{HMD_URL}/api/datastore/dataset/manage/{hmd_id}/fields/"
    headers = {"Content-Type": "application/json; charset=utf-8",
               "authorization": F"Token {apikey}"}
    requests.put(url, headers=headers, json=fields)


def compute_fields(df):
    """
   Retrieve all fields of a dataframe

   df : the dataframe from which we retrieve fields

   Returns:
       The fields to a format understanding by the backend

   """
    fields = []
    for _ in list(df.columns):
        print(F"df[{_}] = {df[_].dtype}")
        field = {"editable": True,
                 "exists": True,
                 "isNew": True,
                 "name": _.lower(),
                 "primary_key": False,
                 "required": False,
                 "type": compute_type(df[_].dtype),
                 "updatedName": _.lower()}
        fields.append(field)
    if df.index.name is not None:
        field = {"editable": True,
                 "exists": True,
                 "isNew": True,
                 "name": df.index.name.lower(),
                 "primary_key": True,
                 "required": True,
                 "type": compute_type(df.index.dtype),
                 "updatedName": df.index.name.lower()}
    else:
        field = {"editable": True,
                 "exists": True,
                 "isNew": True,
                 "name": "index",
                 "primary_key": True,
                 "required": True,
                 "type": compute_type(df.index.dtype),
                 "updatedName": "index"}
    fields.append(field)
    return fields


def compute_value(values):
    """
    Get the "where" part of the query sql to execute in the database

    values : the values which is our filter (value = [{"column":column,"operator":op,"value":value},...])

    Returns:
        a string containing the 'where' part of the query sql to execute
    """
    sql = ""
    if values:
        sql = "WHERE "
        for _ in values:
            if sql != "WHERE ":
                sql += "AND "
            sql += F"{_['column']} {_['operator']} '{_['value']}'"
    return sql


def compute_type(dtype):
    """
   Retrieve the type of field from a dataframe to postgres

   dtype : the type to convert into postgres type

    Returns:
        The type postgres
   """
    if dtype == "float64":
        print("float64")
        return "float"
    if dtype == "int64":
        print("int64")
        return "integer"
    if dtype == "datetime64":
        print("timestamp")
        return "timestamp"
    if dtype == "datetime64[ns]":
        return "timestamp"
    else:
        print("text")
        return "text"


def get_query(db_name, heads=None, limit=None, values=None):
    """
    Get the query sql to execute in the database, retrieve only column present in heads or all column if heads is none

    db_name : the name of the database where we search
    heads : list with columns of interest
    limit : limit the number of rows retrieved
    values : the values which is our filter

    Returns:
        a string containing the query sql to execute
    """
    sql = compute_value(values)
    if heads:
        hd = heads[0]
        for _ in heads[1:]:
            hd += F",{_} "
        query = "SELECT %s FROM %s " % (hd, db_name)
    else:
        query = "SELECT * FROM %s " % db_name
    query += sql
    if limit:
        query += ' LIMIT %s' % limit
    return query
